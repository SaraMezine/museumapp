package fr.poquet.mezine.culturemoi;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import fr.poquet.mezine.culturemoi.modele.ImageDownloader;

public class GaleryActivity extends AppCompatActivity {

    @BindView(R.id.galleryGridView)
    GridView gridViewGalery;
    List<String> uriPicture = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galery);
        ButterKnife.bind(this);

        // récupère l'id du musée
        Intent intent = getIntent();
        String idMuseum = intent.getExtras().getString("idMuseum");

        // récupère les uri des photos à afficher
        ArrayList<String> urlPictures = getIntent().getStringArrayListExtra("urlPictures");
        Log.d("galery", "nombre photo " + urlPictures.size());
        // rempli la grid view
        gridViewGalery.setAdapter(new ImageAdapter(urlPictures, this));
    }


}
