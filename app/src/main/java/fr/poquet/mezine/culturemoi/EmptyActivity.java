package fr.poquet.mezine.culturemoi;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmptyActivity extends AppCompatActivity {

    @BindView(R.id.menu_bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.emptyqr_imageView)
    ImageView emptyQrImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
        ButterKnife.bind(this);
        setBottomNav();
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
    }

    @OnClick(R.id.emptyqr_imageView)
    void onClick(View view){

        boolean connected = checkConnectivity();

        //check des permissions du telephone pour utilisation du qr
        if(connected) {
            if (ActivityCompat.checkSelfPermission(EmptyActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(EmptyActivity.this, QRcodeActivity.class);
                startActivity(intent);
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("Attention")
                        .setMessage("Vous devez autoriser l'accès à la caméra pour utiliser la fonction QR.")
                        .setPositiveButton("Activer", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(EmptyActivity.this, QRcodeActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Annuler", null).show();
            }
        }else{
                new AlertDialog.Builder(this)
                        .setTitle("Attention")
                        .setMessage("Vous devez posseder une connexion internet pour utiliser la fonction QR code.")

                        .setPositiveButton("Paramètres", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                }
                            }
                        })

                        .setNegativeButton("Annuler", null).show();
            }
        }

    //verificaion de la connexion à internet
    private boolean checkConnectivity() {

        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    //Initialisation du menu bottomview
    private void setBottomNav(){

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()){

                            case(R.id.action_home):
                                Intent intent = new Intent(EmptyActivity.this, MainActivity.class);
                                startActivity(intent);
                                break;

                            case(R.id.action_qr):
                                checkConnectivity();
                                break;

                        }
                        return true;
                    }
                }
        );
    }
}
