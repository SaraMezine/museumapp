package fr.poquet.mezine.culturemoi.service;

import android.content.Context;

import com.squareup.moshi.Moshi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class RetrofitInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL_GET = "http://vps449928.ovh.net/v2/api-docs";
    private static final String BASE_URL_POST = "http://vps449928.ovh.net/";


    public static Retrofit getRetrofitInstance(Moshi moshi, OkHttpClient client){
        if(retrofit == null){
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL_GET)
                    .addConverterFactory(MoshiConverterFactory.create(moshi))
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getRetrofitClient(Context context){
        if(retrofit == null){
            OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_POST)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
