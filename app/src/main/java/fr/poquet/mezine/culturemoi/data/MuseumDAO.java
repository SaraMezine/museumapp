package fr.poquet.mezine.culturemoi.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.poquet.mezine.culturemoi.modele.Museum;

public class MuseumDAO {

    private SQLiteDatabase db; // base de donnée
    private SQLiteOpenHelper museumDBOpenHelper; // l'open helper
    private Context context;
    private String[] columnsTable = {MuseumDBOpenHelper.DB_COLUMN_ID, MuseumDBOpenHelper.DB_COLUMN_ID_MUSEE,
            MuseumDBOpenHelper.DB_COLUMN_NOM,MuseumDBOpenHelper.DB_COLUMN_PERIODE_OUV,
            MuseumDBOpenHelper.DB_COLUMN_ADRESSE, MuseumDBOpenHelper.DB_COLUMN_VILLE, MuseumDBOpenHelper.DB_COLUMN_FERME,
            MuseumDBOpenHelper.DB_COLUMN_FERM_ANNUELLE, MuseumDBOpenHelper.DB_COLUMN_SITE_WEB, MuseumDBOpenHelper.DB_COLUMN_CP,
            MuseumDBOpenHelper.DB_COLUMN_REGION, MuseumDBOpenHelper.DB_COLUMN_DEPT};
    private static final String TAG = "MuseumDAO";

    public MuseumDAO(Context context) {
        this.context = context;
        this.museumDBOpenHelper = new MuseumDBOpenHelper(context);
    }

    public void open(){
        db = museumDBOpenHelper.getWritableDatabase();
    }


    //verifier si bdd vide
    public boolean emptyBDD(){

        SQLiteDatabase dbr = museumDBOpenHelper.getReadableDatabase();
        Cursor cursor = dbr.query(MuseumDBOpenHelper.DB_TABLE_NAME, columnsTable,
                null,
                null,
                null,
                null,
                null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()){
            Log.d(TAG, "BDD vide" );
            dbr.close();
            return true;
        }else{
            Log.d(TAG, "BDD non vide" );
            dbr.close();
            return false;
        }
    }


    // ajoute un musée à la database
    public void addMuseum(Museum museum){

        this.open();
        ContentValues cv = new ContentValues();
        Log.d(TAG, "get id musee " + museum.getIdMusee());

        cv.put(MuseumDBOpenHelper.DB_COLUMN_ID_MUSEE, museum.getIdMusee());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_NOM, museum.getName());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_PERIODE_OUV, museum.getPeriode_ouv());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_ADRESSE, museum.getAdresse());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_VILLE, museum.getVille());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_FERME, museum.getFerme());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_FERM_ANNUELLE, museum.getFerme_annuelle());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_SITE_WEB, museum.getSite_web());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_CP, museum.getCp());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_REGION, museum.getRegion());
        cv.put(MuseumDBOpenHelper.DB_COLUMN_DEPT, museum.getDept());

        long nb = db.insert(MuseumDBOpenHelper.DB_TABLE_NAME, null, cv);
        db.close();
    }

    // recupere tous les musées de la database

    public List<Museum> getMuseums(){

        List<Museum> Museums = new ArrayList<>();

        SQLiteDatabase db = museumDBOpenHelper.getReadableDatabase();

        Cursor cursor = db.query(MuseumDBOpenHelper.DB_TABLE_NAME, columnsTable, null,null,null,null,null);

        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Museum museum = new Museum();
            museum.setId(cursor.getString(0));
            Log.d(TAG, "id musee =" + cursor.getString(1));
            museum.setIdMusee(cursor.getString(1));
            museum.setName(cursor.getString(2));
            museum.setPeriode_ouv(cursor.getString(3));
            museum.setAdresse(cursor.getString(4));
            museum.setVille(cursor.getString(5));
            String ferme = cursor.getString(6);
            if(ferme == "true"){
                museum.setFerme(true);
            }else{
                museum.setFerme(false);
            }
            museum.setFerme_annuelle(cursor.getString(7));
            museum.setSite_web(cursor.getString(8));
            museum.setCp(cursor.getString(9));
            museum.setRegion(cursor.getString(10));
            museum.setDept(cursor.getString(11));

            Museums.add(museum); // ajoute le musée recuperer a la liste des musées

            cursor.moveToNext();
        }
        db.close();
        return Museums;
    }

    // recupere tous les ids des musées de la database
    public List<String> getMuseumsID(){

        SQLiteDatabase db = museumDBOpenHelper.getReadableDatabase();

        List<String> id = new ArrayList<>();
        this.open();

        Cursor cursor = db.query(MuseumDBOpenHelper.DB_TABLE_NAME, columnsTable, null,null,null,null,null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            id.add(cursor.getString(1));
            Log.d(TAG, "id Musee = " + cursor.getString(1));
            cursor.moveToNext();
        }
        db.close();
        return id;
    }

    // A COMPLETER pour recuperer le musée dont l'id correspond à l'argument idMusee
    public Museum getMuseum(String idMusee){

        Museum museum = new Museum();
        SQLiteDatabase db = museumDBOpenHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM "+MuseumDBOpenHelper.DB_TABLE_NAME+" WHERE "+MuseumDBOpenHelper.DB_COLUMN_ID_MUSEE+" = '"+idMusee.trim()+"'", null);

        //Cursor cursor = db.query(MuseumDBOpenHelper.DB_TABLE_NAME, columnsTable, null ,null,null,null,null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            museum.setId(cursor.getString(0));
            Log.d(TAG, "id musee =" + cursor.getString(1));
            museum.setIdMusee(cursor.getString(1));
            museum.setName(cursor.getString(2));
            museum.setPeriode_ouv(cursor.getString(3));
            museum.setAdresse(cursor.getString(4));
            museum.setVille(cursor.getString(5));
            String ferme = cursor.getString(6);
            if(ferme == "true"){
                museum.setFerme(true);
            }else{
                museum.setFerme(false);
            }
            museum.setFerme_annuelle(cursor.getString(7));
            museum.setSite_web(cursor.getString(8));
            museum.setCp(cursor.getString(9));
            museum.setRegion(cursor.getString(10));
            museum.setDept(cursor.getString(11));

            cursor.moveToNext();
        }
        db.close();
        return museum;
    }
}
