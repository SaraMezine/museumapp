package fr.poquet.mezine.culturemoi.modele;

public class Museum {

    private String id;
    private String name;
    private String periode_ouv;
    private String adresse;
    private String ville;
    private Boolean ferme;
    private String ferme_annuelle;
    private String site_web;
    private String cp;
    private String region;
    private String dept;
    private String idMusee;

    public Museum(String name, String periode_ouv, String adresse, String ville, Boolean ferme,
                  String ferme_annuelle, String site_web, String cp, String region, String dept) {
        this.name = name;
        this.periode_ouv = periode_ouv;
        this.adresse = adresse;
        this.ville = ville;
        this.ferme = ferme;
        this.ferme_annuelle = ferme_annuelle;
        this.site_web = site_web;
        this.cp = cp;
        this.region = region;
        this.dept = dept;
    }

    public Museum(String name) {
        this.name = name;
    }

    public Museum(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriode_ouv() {
        return periode_ouv;
    }

    public void setPeriode_ouv(String periode_ouv) {
        this.periode_ouv = periode_ouv;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Boolean getFerme() {
        return ferme;
    }

    public void setFerme(Boolean ferme) {
        this.ferme = ferme;
    }

    public String getFerme_annuelle() {
        return ferme_annuelle;
    }

    public void setFerme_annuelle(String ferme_annuelle) {
        this.ferme_annuelle = ferme_annuelle;
    }

    public String getSite_web() {
        return site_web;
    }

    public void setSite_web(String site_web) {
        this.site_web = site_web;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getIdMusee() {
        return idMusee;
    }

    public void setIdMusee(String idMusee) {
        this.idMusee = idMusee;
    }
}
