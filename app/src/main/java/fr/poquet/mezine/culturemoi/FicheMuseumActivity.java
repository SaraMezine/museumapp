package fr.poquet.mezine.culturemoi;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.moshi.Moshi;
import org.json.JSONArray;
import org.json.JSONException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.poquet.mezine.culturemoi.modele.ImageDownloader;
import fr.poquet.mezine.culturemoi.data.MuseumDAO;
import fr.poquet.mezine.culturemoi.modele.Museum;
import fr.poquet.mezine.culturemoi.service.MoshiMuseumAdapter;
import fr.poquet.mezine.culturemoi.service.MuseumService;
import fr.poquet.mezine.culturemoi.service.RetrofitInstance;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class FicheMuseumActivity extends AppCompatActivity {

    private static final int REQUEST_TAKE_PHOTO = 2;
    @BindView(R.id.fiche_nom_textView)
    TextView ficheNomTV;
    @BindView(R.id.fiche_periodeOuverture_textView)
    TextView fichePeriodeOuvertureTV;
    @BindView(R.id.fiche_fermetureAnnuelle_textView)
    TextView ficheFermetureAnnuelleTV;
    @BindView(R.id.fiche_adresseRue_textView)
    TextView ficheAdresseRueTV;
    @BindView(R.id.fiche_adresseVille_textView)
    TextView ficheAdresseVileTV;
    @BindView(R.id.fiche_adresseCp_textView)
    TextView ficheAdresseCpTV;
    @BindView(R.id.fiche_adresseDepartement_textView)
    TextView ficheAdresseDeptTV;
    @BindView(R.id.fiche_adresseRegion_textView)
    TextView ficheAdresseRegionTV;
    @BindView(R.id.fiche_site_textView)
    TextView ficheSiteTV;
    @BindView(R.id.fiche_ouvertFerme_textView)
    TextView ficheOuvertFermeTV;
    @BindView(R.id.menu_bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.fiche_addPhoto_button)
    Button ficheAddPhotoButton;
    @BindView(R.id.fiche_image_imageView)
    ImageView ficheImageIV;
    @BindView(R.id.fiche_takePhoto_button)
    Button fiche_takePhoto_button;
    @BindView(R.id.fiche_goto_galery)
    Button fiche_goto_galery;
    private Bitmap mImageBitmap;
    private String mCurrentPhotoPath;

    private static final String TAG = "FicheMuseumActivity";

    Museum museum = new Museum();
    ImageDownloader imagedo;
    List<String> urlPictures = new ArrayList<>();
    MuseumDAO museumDAO;
    String upImageUriString;
    String idMuseum;
    String urlPicture;
    private SlidrInterface slidr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fiche_museum);
        ButterKnife.bind(this);

        slidr = Slidr.attach(this);
        upImageUriString = "";
        museumDAO = new MuseumDAO(this);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        // s'il n'y a pas de permission de camera
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ficheAddPhotoButton.setEnabled(false);
            fiche_takePhoto_button.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }

        Intent intent = getIntent();
        String from = intent.getExtras().getString("FROM"); // deux manières d'acceder à la fiche museum

        switch (from){

            case "QRCodeActivity":

                String url_museum = intent.getExtras().getString("url_musee");
                String prefix = "http://vps449928.ovh.net/api/musees/";
                idMuseum= url_museum.substring(url_museum.indexOf(prefix) + prefix.length());
                getMuseumAPI(idMuseum); // appel API et sauvegarde du musée dans la BDD
                urlPicture = url_museum + "/pictures"; // recupere l'url de la liste des photos
                Log.d(TAG, "rechargement liste photo url = " + urlPicture);
                //showPicture(idMuseum);
                getListPictures(urlPicture);

            break;

            case "MainActivity":

                idMuseum = intent.getExtras().getString("idMusee");
                Log.d(TAG, "idmusee = "+ idMuseum);
                museum = museumDAO.getMuseum(idMuseum);
                majInfoMuseum(museum);
                showPicture(museum.getIdMusee());
                urlPicture = "http://vps449928.ovh.net/api/musees/" + idMuseum + "/pictures";
                getListPictures(urlPicture);
                break;
        }

        /*// n'autorise pas la consultation de photo ou l'envoie de photo si pas de connexion internet
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            fiche_takePhoto_button.setEnabled(true);
            fiche_goto_galery.setEnabled(true);
            ficheAddPhotoButton.setEnabled(true);
        }
        else{
            fiche_takePhoto_button.setEnabled(false);
            fiche_goto_galery.setEnabled(false);
            ficheAddPhotoButton.setEnabled(false);
        }*/

        setBottomNav();
    }

    //affichage de l'image principale du musee aleatoire parmi les images du musee
    private void showPicture(String idMusee) {

        ImageDownloader imd = new ImageDownloader();

        String path = imd.getDirName(idMusee);
        File directory = new File(path);
        File[] files = directory.listFiles();

        if(files.length>0){
        Random r = new Random();
        int valeur = 1 + r.nextInt(files.length -1);
        Log.d("Files", "random number = " + valeur);
        File interfile = files[valeur];
        String completePath = path +"/" + interfile.getName() ;

        File file = new File(completePath);
        Uri imageUri = Uri.fromFile(file);

        Glide.with(FicheMuseumActivity.this)
                .load(imageUri)
                .into(ficheImageIV);
        }else{
            // mettre une photo nulle si pas d'image ?
        }
    }

    //set des informations du musee dans les tv
    private void majInfoMuseum(Museum musee) {

        ficheNomTV.setText(musee.getName());
        fichePeriodeOuvertureTV.setText(musee.getPeriode_ouv());
        ficheFermetureAnnuelleTV.setText(musee.getFerme_annuelle());
        ficheAdresseRueTV.setText(musee.getAdresse());
        ficheAdresseVileTV.setText(musee.getVille());
        ficheAdresseCpTV.setText(musee.getCp());
        ficheAdresseDeptTV.setText(musee.getDept());
        ficheAdresseRegionTV.setText(musee.getRegion());
        ficheSiteTV.setText(musee.getSite_web());
        if(musee.getFerme()){
            ficheOuvertFermeTV.setText("Fermé");
            ficheOuvertFermeTV.setBackgroundColor(Color.RED);
        }else{
            ficheOuvertFermeTV.setText("Ouvert");
            ficheOuvertFermeTV.setBackgroundColor(getResources().getColor(R.color.colorGreen));
        }
    }

    //sauvegarde des photos dans le storage de l'appareil
    private void sauvegardePhotos(String idMuseum, String photoName) {

        for( int i=0; i < urlPictures.size(); i++){

            String url = urlPictures.get(i);
            imagedo = new ImageDownloader(url); // donner l'url de l'image a charger
            imagedo.imageDownload(this, idMuseum, photoName); // idPhoto + numeroJPEG

        }

    }

    //verification pour voir si id est deja dans la bdd
    private boolean checkIdBDD(String id){
        List<String> ids;
        if(!museumDAO.emptyBDD()){

            ids = museumDAO.getMuseumsID();

            for (int i = 0 ; i < ids.size() ; i++){
                if (ids.get(i).equals(id)){
                    return true;
                }
            }
        }
        return false;
    }

    //recuperer le musee en fonction de son id avec api
    private void getMuseumAPI(String id){

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY); // Pour loger les requetes et reponses

        final OkHttpClient okhttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor).build();

        Moshi moshi = new Moshi.Builder().add(new MoshiMuseumAdapter()).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://vps449928.ovh.net/")
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .client(okhttpClient).build();

        MuseumService service = retrofit.create(MuseumService.class);
        Call<Museum> museumCall = service.getMuseumWithID(id);

        museumCall.enqueue(new Callback<Museum>() {
            @Override
            public void onResponse(Call<Museum> call, Response<Museum> response) {
                museum = response.body();
                Log.d(TAG, "onResponse" + museum.getName());
                String id = museum.getIdMusee();
                MuseumDAO museumDAO = new MuseumDAO(FicheMuseumActivity.this);

                if(!checkIdBDD(id)){
                    museumDAO.addMuseum(museum);
                    Log.d(TAG, "ajout musée a la bdd " +  museum.getName());
                }
                majInfoMuseum(museum);
            }

            @Override
            public void onFailure(Call<Museum> call, Throwable t) {
                Log.e(TAG, "onFailure", t);
            }
        });

    }

    //recuperer les images en fonction de l'url du musee
    private void getListPictures(String url2){

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url2,
                null,
                new com.android.volley.Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        Log.d(TAG, "nombre de photos" + response.length());
                        for(int i=0; i< response.length(); i++){
                            String urlPicture = null;
                            try {
                                urlPicture = response.getString(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            urlPictures.add(urlPicture);
                        }
                            try {
                                String urlPicture = response.getString(0);

                                urlPictures.add(urlPicture);

                                String prefix = "http://vps449928.ovh.net/api/musees/";
                                String id = urlPicture.substring(urlPicture.indexOf(prefix) + prefix.length(), urlPicture.indexOf(prefix) + prefix.length()+ 40);
                                String prefix2 =  "http://vps449928.ovh.net/api/musees/" + id +"/pictures/";
                                String photoName = urlPicture.substring(urlPicture.indexOf(prefix2) + prefix2.length());

                                ImageDownloader imd = new ImageDownloader();
                                String path = imd.getDirName(id);
                                String completePath = path +"/" + photoName ;

                                File file = new File(completePath);
                                Uri imageUri = Uri.fromFile(file);

                                Log.d(TAG, "photo a display " + response.getString(0) );

                                Glide.with(FicheMuseumActivity.this)
                                        .load(response.getString(0))
                                        .into(ficheImageIV);

                                if(isExternalStorageWritable()){
                                    sauvegardePhotos(id, photoName);
                                }else{
                                    Toast.makeText(FicheMuseumActivity.this, "Vous n'avez pas assez d'espace pour sauvegarder les photos", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "error liste" + error);

            }
        });

        requestQueue.add(jsonArrayRequest);
    }


    //check de l'external stockage pour la sauvegarde des photos
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    @OnClick(R.id.fiche_takePhoto_button)
    public void take_photo(){

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         File photoFile = getOutputMediaFile();
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "fr.poquet.mezine.culturemoi.fileprovider", photoFile);
                takePictureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, photoURI);
                setResult(RESULT_OK, takePictureIntent);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
        }

    }

    private File getOutputMediaFile(){
        // Create an image file name
        String timeStamp = new SimpleDateFormat("dd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(imageFileName,".jpg",storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
        }

    @OnClick(R.id.fiche_addPhoto_button)
    public void add_photo(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/jpg");
        startActivityForResult(intent, 0);

    }

    @OnClick(R.id.fiche_goto_galery)
    public void gotoGalery(){
        Intent galeryIntent = new Intent(FicheMuseumActivity.this, GaleryActivity.class);
        galeryIntent.putExtra("idMuseum", idMuseum);
        galeryIntent.putStringArrayListExtra("urlPictures", (ArrayList<String>) urlPictures);
        startActivityForResult(galeryIntent, 1);
    }

   //recuperer une photo du tel et afficher sur fiche
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2 && resultCode == RESULT_OK) { // si on prend une photo

            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(mCurrentPhotoPath));
                Log.d(TAG, "mcurrentpath " + mCurrentPhotoPath);
                File newfile = getOutputMediaFile();
                if (newfile.exists()) {
                    newfile.delete ();
                }
                try {
                    FileOutputStream out = new FileOutputStream(newfile);
                    mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Uri imageFilePath = Uri.parse(mCurrentPhotoPath);

                File f = new File(imageFilePath.getPath());

                if(!f.isFile()){
                    Log.d(TAG,"Le fichier image n'existe pas");
                }else {
                    Log.d(TAG, "Le fichier existe " + f.getAbsolutePath());
                    uploadToServer(f);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            } else if( requestCode == 0){  // si on telecharge une photo depuis le telephone

                if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                    Uri imageFilePath = data.getData(); //content://media/external/images/media/159
                    final String path = getPathFromURI(imageFilePath); ///storage/emulated/0/Download/Sortir-Bouger-Culture-Musées-d’Alès.jpg
                    if (path != null) {
                        File f = new File(path);
                        imageFilePath = Uri.fromFile(f);

                        if(!f.isFile()){
                            Log.d(TAG,"Le fichier image n'existe pas");
                        }else{
                            Log.d(TAG,"Le fichier existe " + f.getAbsolutePath());
                            uploadToServer(f);
                        }
                    }
                }
            }

        String prefix = "http://vps449928.ovh.net/api/musees/";
        getMuseumAPI(idMuseum); // appel API et sauvegarde du musée dans la BDD
        String url = prefix + idMuseum  + "/pictures";
        Log.d(TAG, "url pour mise a jour des photos");
        getListPictures(url); // mise a jour des photos enregistré

    }

        public String getPathFromURI(Uri contentUri) {
            String res = null;
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                res = cursor.getString(column_index);
            }
            cursor.close();
            return res;
        }

    //envoi de l'image à partir de son Uri
    private void uploadToServer(File file) {

        Retrofit retrofit = RetrofitInstance.getRetrofitClient(this);

        MuseumService museumService = retrofit.create(MuseumService.class);

        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/jpg"), file);

        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);

        Call call = (Call) museumService.uploadImage(museum.getIdMusee(),part);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int code = response.code();
                if(code == 200){

                    Toast.makeText(FicheMuseumActivity.this, "L'image a bien été chargée", Toast.LENGTH_SHORT).show();
                    majInfoMuseum(museum);
                    String prefix = "http://vps449928.ovh.net/api/musees/";
                    getMuseumAPI(museum.getIdMusee()); // appel API et sauvegarde du musée dans la BDD
                    String url = prefix + museum.getId()  + "/pictures";
                    Log.d(TAG, "reponse " + response);

                }else{

                    new AlertDialog.Builder(FicheMuseumActivity.this)
                            .setTitle("Attention")
                            .setMessage("Votre image n'a pas pu être téléchargée.")

                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d(TAG, "on failure telechargement de la photo ");
                Log.d(TAG, "erreur =  " + t);

                new AlertDialog.Builder(FicheMuseumActivity.this)
                        .setTitle("Attention")
                        .setMessage("Votre image n'a pas pu être téléchargée.")

                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });

        String prefix = "http://vps449928.ovh.net/api/musees/";
        getMuseumAPI(idMuseum); // appel API et sauvegarde du musée dans la BDD
        String url = prefix + idMuseum  + "/pictures";
        Log.d(TAG, "url pour mise a jour des photos");
        getListPictures(url); // mise a jour des photos enregistré

    }

    //Initialisation du menu bottomview
    private void setBottomNav(){

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()){

                            case(R.id.action_home):
                                Intent intent = new Intent(FicheMuseumActivity.this, MainActivity.class);
                                startActivity(intent);
                                break;

                            case(R.id.action_qr):
                                Intent intent2 = new Intent(FicheMuseumActivity.this, QRcodeActivity.class);
                                startActivity(intent2);
                                break;
                        }
                        return true;
                    }
                }
        );
    }

}

