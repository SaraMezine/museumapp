package fr.poquet.mezine.culturemoi;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import java.util.List;

public class ImageAdapter extends BaseAdapter {

    private List<String> listUrlPhotos;
    private Context context ;

    public ImageAdapter(List<String> listUrlPhotos, Context context) {
        this.listUrlPhotos = listUrlPhotos;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listUrlPhotos.size();
    }

    @Override
    public String getItem(int i) {
        return listUrlPhotos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    //set de l'image view et affectation de l'image
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ImageView imageView;
        if (view == null) {
            imageView = new ImageView(this.context);
            imageView.setLayoutParams(new GridView.LayoutParams(200, 115));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) view;
        }

        Glide.with(context)
                .load(listUrlPhotos.get(i))
                .into(imageView);

        return imageView;
    }

}
