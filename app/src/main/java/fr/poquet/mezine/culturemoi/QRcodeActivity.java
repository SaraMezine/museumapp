package fr.poquet.mezine.culturemoi;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrInterface;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
public class QRcodeActivity extends AppCompatActivity {

    @BindView(R.id.QR_surfaceView)
    SurfaceView qrSV;
    @BindView(R.id.QRValue_textView)
    TextView qrValueTV;
    @BindView(R.id.menu_bottom_navigation)
    BottomNavigationView bottomNavigationView;

    private static final String TAG = "QRcodeActivity";

    private String intentData = "";
    private SlidrInterface slidr;


    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        ButterKnife.bind(this);
        setBottomNav();
        bottomNavigationView.getMenu().findItem(R.id.action_qr).setChecked(true);
        slidr = Slidr.attach(this);

        /*pour tests ludivine
        intentData = "http://vps449928.ovh.net/api/musees/ff7b6cd8c9ca7ace762253db33dbe31f42f98e38";
        //intentData = "http://vps449928.ovh.net/api/musees/5c637e3c61e55c808b31e1ae12a57fc5c4842b4b";
        //intentData = "http://vps449928.ovh.net/api/musees/1541cee1812667c2e85cc857ae7e2501f171f236";
        //intentData = "http://vps449928.ovh.net/api/musees/014176fde2dc4ba06376bd221f9521dc424d309f";
        //intentData = "http://vps449928.ovh.net/api/musees/00a65b5e4bb5f81a9ad21ea861a8a32ebf93a3ae";
        //intentData = "http://vps449928.ovh.net/api/musees/04b3c5032a9548b798fa0327ae6939d6b4127758";
        Intent intent = new Intent(QRcodeActivity.this, FicheMuseumActivity.class);
        intent.putExtra("url_musee", intentData);
        intent.putExtra("FROM", "QRCodeActivity");
        startActivity(intent);*/
    }

    //Activation de la caméra et du détecteur de qr code
    private void initialiseDetectorsAndSources() {

        Toast.makeText(getApplicationContext(), "Scanner QR code activé", Toast.LENGTH_SHORT).show();

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true)
                .build();

        qrSV.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(QRcodeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(qrSV.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(QRcodeActivity.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {

                    qrValueTV.post(new Runnable() {

                        @Override
                        public void run() {

                                intentData = barcodes.valueAt(0).displayValue;
                                Intent intent = new Intent(QRcodeActivity.this, FicheMuseumActivity.class);
                                intent.putExtra("url_musee", intentData);
                                Log.d(TAG, "url musee = "+ intentData);
                                intent.putExtra("FROM", "QRCodeActivity");
                                startActivity(intent);

                        }
                    });

                }
            }
        });
    }

    //Action camera quand app en pause
    @Override
    protected void onPause() {
        super.onPause();
        cameraSource.release();
    }

    //Action de camera quand app recommence
    @Override
    protected void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();
    }

    //Initialisation du menu bottomview
    private void setBottomNav(){

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()){

                            case(R.id.action_home):
                                Intent intent = new Intent(QRcodeActivity.this, MainActivity.class);
                                startActivity(intent);
                                break;

                            case(R.id.action_qr):
                                Intent intent2 = new Intent(QRcodeActivity.this, QRcodeActivity.class);
                                startActivity(intent2);
                                break;
                        }
                        return true;
                    }
                }
        );
    }
}

