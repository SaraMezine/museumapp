package fr.poquet.mezine.culturemoi;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.poquet.mezine.culturemoi.modele.ImageDownloader;
import fr.poquet.mezine.culturemoi.modele.Museum;

public class MuseumAdapter extends RecyclerView.Adapter<MuseumAdapter.MuseumHolder>{

        private static final String TAG = "MuseeAdapter";

        private Context context;
        private List<Museum> museums;

        public MuseumAdapter(List<Museum> museums, Context context) {
            this.museums = museums;
            this.context = context;
        }

        //Creation du view holder
        @Override
        public MuseumHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View view = inflater.inflate(R.layout.holder_musee,viewGroup,false);
            Log.d(TAG, "onCreateViewHolder: set ");
            return new MuseumHolder(view);
        }

        //Passage du musee dans le holder
        @Override
        public void onBindViewHolder(@NonNull MuseumHolder museumHolder, int i) {
            Museum museum = museums.get(i);
            museumHolder.setMuseum(museum);
            Log.d(TAG, "onBindViewHolder: le musee" + museum.getName() + "passe dans le holder");
        }

        @Override
        public int getItemCount() {
            return museums.size();
        }

        //definition du holder d'un musee
        class MuseumHolder extends RecyclerView.ViewHolder {

            private Museum museum;

            @BindView(R.id.holderMuseum_nom_textView)
            TextView holderMuseumNomTV;
            @BindView(R.id.holderMuseum_background_imageView)
            ImageView holderMuseumBackgroundIV;

            MuseumHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this,itemView);
            }

            //affectation des valeurs dans holder
            void setMuseum(Museum museum) {

                this.museum = museum;

                String nom = museum.getName();
                holderMuseumNomTV.setText(nom);

                String idMuseum = museum.getIdMusee();

                ImageDownloader imd = new ImageDownloader();
                String path = imd.getDirName(idMuseum);
                File directory = new File(path);
                File[] files = directory.listFiles();

                Log.d("Files", "Size: "+ files.length);
                File interfile = files[0];
                String completePath = path +"/" + interfile.getName() ;
                Uri imageUri = Uri.fromFile(new File(completePath));

                Glide.with(context)
                        .load(imageUri)
                            .into(holderMuseumBackgroundIV);

                String prefix2 =  "http://vps449928.ovh.net/api/musees/" + idMuseum +"/pictures/";

            }


            //Affichage de la fiche musee en fonction de l'idmusee de la cardview
            @OnClick(R.id.museum_cardview)
            void onClick(View view){
                Context context=view.getContext();
                Intent intent = new Intent(context, FicheMuseumActivity.class);
                intent.putExtra("FROM", "MainActivity");
                intent.putExtra("idMusee", museum.getIdMusee());
                context.startActivity(intent);
            }
        }
    }


