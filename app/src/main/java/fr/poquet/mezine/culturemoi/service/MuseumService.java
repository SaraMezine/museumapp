package fr.poquet.mezine.culturemoi.service;

import fr.poquet.mezine.culturemoi.modele.Museum;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface MuseumService {

    @GET("api/musees/{id}")
    Call<Museum> getMuseumWithID(@Path("id") String id); // le flux doit etre interpreter comme un result d'abord,
    // ensuite comme une liste*

    @Multipart
    @POST("api/musees/{id}/pictures")
    Call<ResponseBody> uploadImage(@Path("id") String id,@Part MultipartBody.Part file);

}
