package fr.poquet.mezine.culturemoi.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

public class MuseumDBOpenHelper extends SQLiteOpenHelper {

    public static final String DB_NAME_MUSEUM_APP = "CultureEtMoi";

    public static final String DB_TABLE_NAME = "Museum";
    public static final int DB_VERSION = 1;

    public static final String DB_COLUMN_ID = "_id";
    public static final String DB_COLUMN_ID_MUSEE = "id_musee";
    public static final String DB_COLUMN_NOM = "nom";
    public static final String DB_COLUMN_PERIODE_OUV = "periode_ouv";
    public static final String DB_COLUMN_ADRESSE = "adresse";
    public static final String DB_COLUMN_VILLE ="ville";
    public static final String DB_COLUMN_FERME="ferme";
    public static final String DB_COLUMN_FERM_ANNUELLE = "ferm_annuelle";
    public static final String DB_COLUMN_SITE_WEB ="site_web";
    public static final String DB_COLUMN_CP = "cp";
    public static final String DB_COLUMN_REGION = "region";
    public static final String DB_COLUMN_DEPT ="dept";
    private static final String TAG = "DBOOpenHelper";

    private static final String DATABASE_CREATE = "create table " + DB_TABLE_NAME + "( "+ DB_COLUMN_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT, " + DB_COLUMN_ID_MUSEE + " TEXT, " + DB_COLUMN_NOM + " TEXT ," + DB_COLUMN_PERIODE_OUV +
            " TEXT ," + DB_COLUMN_ADRESSE + " TEXT ," + DB_COLUMN_VILLE + " TEXT ," + DB_COLUMN_FERME + " TEXT ," +
            DB_COLUMN_FERM_ANNUELLE + " TEXT ," + DB_COLUMN_SITE_WEB + " TEXT ," + DB_COLUMN_CP + " TEXT ," +
            DB_COLUMN_REGION + " TEXT ," + DB_COLUMN_DEPT + " TEXT );";

    //constructeur
    public MuseumDBOpenHelper(@Nullable Context context) {
        super(context, DB_NAME_MUSEUM_APP, null, DB_VERSION);
        Log.d(TAG, "creationBDD");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int lastVersion, int newVersion) {
        Log.w(MuseumDBOpenHelper.class.getName(),
                "Upgrading database from version " + lastVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        onCreate(db);
    }
}
