package fr.poquet.mezine.culturemoi.modele;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class ImageDownloader {

    private String urlPicture;

    private static final String TAG = "ImageDownloader";

    public ImageDownloader(String url) {
        this.urlPicture = url;
    }

    public ImageDownloader() {
    }

    public String getUrlPicture() {
        return urlPicture;
    }

    public void setUrlPicture(String urlPicture) {
        this.urlPicture = urlPicture;
    }

    public  void imageDownload(Context context, String idMuseum, String photoName) {
        try {
            Picasso.with(context)
                    .load(urlPicture)
                    .into(getTarget(getDirName(idMuseum), photoName));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"exeption = " + e );
        }
    }

    // recuperer l'URI de la photo sauvegardée avec l'id
    // de la photo
    public  String getDirName(String idMuseum) {

        File storageDirectory = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES) +"/MuseesPicture/ " + idMuseum)  ;

        boolean create = true;
        if(!storageDirectory.exists()){
            create = storageDirectory.mkdirs();
        }

        Log.d(TAG, "create = "+ create);

        return storageDirectory.getPath();
    }

    private Target getTarget(final String dirName, final String photoName) { //fileName == path
        Target target = new Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {


                            Random generator = new Random();
                            int n = generator.nextInt(10000);

                            File imagefile = new File(dirName , photoName);
                            String savedImagePath = imagefile.getAbsolutePath();
                            Log.d(TAG, "getTarget/ savedImagePath = " + savedImagePath);

                            if (imagefile.exists()) {
                                imagefile.delete();
                            }

                            imagefile.createNewFile();
                            FileOutputStream fileoutputstream = new FileOutputStream(imagefile);
                            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 60, bytearrayoutputstream);
                            fileoutputstream.write(bytearrayoutputstream.toByteArray());
                            fileoutputstream.close();

                            Log.d(TAG, "Chargement de l'image fini");
                        } catch (IOException e) {
                            Log.e(TAG, e.getLocalizedMessage());
                        }
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }

    public static String getFileFullPath(String fileName) {
        try {
            if (fileName != null && !fileName.isEmpty()) {
                String base = (Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getPath());
                return "/" + base + "/MuseesPicture/" + fileName;
            } else return "";
        } catch (Exception e) {
            return "";
        }
    }



}
