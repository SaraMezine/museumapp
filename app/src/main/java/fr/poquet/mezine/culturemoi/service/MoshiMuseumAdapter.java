package fr.poquet.mezine.culturemoi.service;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.JsonReader;

import java.io.IOException;

import fr.poquet.mezine.culturemoi.modele.Museum;

public class MoshiMuseumAdapter /*implements JsonAdapter.Factory */{

    @FromJson
    public Museum fromJSON(JsonReader in) throws IOException {
        Museum musee = new Museum();

        in.beginObject();
        while(in.hasNext()){

            String name = in.nextName();

            switch (name){
                case "adresse":
                    musee.setAdresse(in.nextString());
                    break;

                case"cp":
                    musee.setCp(in.nextString());
                    break;

                case "dept":
                    musee.setDept(in.nextString());
                    break;

                case "ferme":
                    musee.setFerme(in.nextBoolean());
                    break;

                case "fermeture_annuelle":
                    musee.setFerme_annuelle(in.nextString());
                    break;

                case "id":
                    musee.setIdMusee(in.nextString());
                    break;

                case "nom":
                    musee.setName(in.nextString());
                    break;

                case "periode_ouverture":
                    musee.setPeriode_ouv(in.nextString());
                    break;

                case "region":
                    musee.setRegion(in.nextString());
                    break;

                case "site_web":
                    musee.setSite_web(in.nextString());
                    break;

                case "ville":
                    musee.setVille(in.nextString());
                    break;

                default:
                    in.skipValue();
            }
        }
        in.endObject();
        return musee;
    }

}
