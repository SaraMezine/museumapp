package fr.poquet.mezine.culturemoi;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.poquet.mezine.culturemoi.data.MuseumDAO;
import fr.poquet.mezine.culturemoi.modele.Museum;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.menu_bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.museum_recyclerView)
    RecyclerView museumRV;

    private static final String TAG = "MainActivity";

    MuseumDAO museumDAO;
    RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        museumRV.setLayoutManager(layoutManager);
        museumDAO = new MuseumDAO(this);

        boolean emptyBDD = museumDAO.emptyBDD();
        if (emptyBDD){
            Intent intent = new Intent(MainActivity.this, EmptyActivity.class);
            startActivity(intent);

        }else{

            List<Museum> museums = museumDAO.getMuseums();
            Collections.reverse(museums);
            adapter = new MuseumAdapter(museums, this);
            museumRV.setAdapter(adapter);
        }
        setBottomNav();
    }

    //Initialisation du menu bottomview
    private void setBottomNav(){

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()){

                            case(R.id.action_home):
                                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                startActivity(intent);
                                break;

                            case(R.id.action_qr):
                                Intent intent2 = new Intent(MainActivity.this, QRcodeActivity.class);
                                startActivity(intent2);
                                break;

                        }
                        return true;
                    }
                }
        );
    }

}
